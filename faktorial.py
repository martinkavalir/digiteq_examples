"""Prezentace výpočtu faktoriálu."""


def vypocet_faktorial(_n):
    """Slouží k výpočtu faktoriálu. Zadej kladné číslo n, pro které chceš vypočíst faktoriál."""
    if _n < 0:
        return -1
    for i in range(0, _n + 1):
        if i == 0:
            faktorial = 1
        else:
            faktorial *= i
    return faktorial


print(vypocet_faktorial(10))
