"""Příklad na hanojské věže pro možnost 4 tyčí a 6 disků."""


def hanojske_veze_3(pocet_disku, start=1, cil=3, tyce=None):
    """Slouží k výpočtu případu 3 věží."""
    if tyce is None:
        tyce = {1, 2, 3}
    if pocet_disku == 0 or start == cil:
        return []
    krok = "(%s,%s)" % (start, cil)
    if pocet_disku == 1:    # pro případ, že zbývá jen 1 disk
        return [krok]
    odkladaci_tyc = tyce.difference([start, cil]).pop()     # tyč, která slouží k odkládání disku
    prvni_cast = hanojske_veze_3(pocet_disku-1, start, odkladaci_tyc)
    posledni_cast = hanojske_veze_3(pocet_disku-1, odkladaci_tyc, cil)
    return prvni_cast + [krok] + posledni_cast


def hanojske_veze_4(pocet_disku, start=1, cil=2, tyce=None):
    """Slouží k výpočtu případu 4 věží."""
    if tyce is None:
        tyce = {1, 2, 3, 4}
    if pocet_disku == 0 or start == cil:        # Pokud není žádný disk, není žádný pohyb
        return []
    if pocet_disku == 1 and len(tyce) > 1:      # V případě jednoho disku je jen 1 pohyb
        return ["(%s,%s)" % (start, cil)]
    if len(tyce) == 3:      # Jde do algoritmu pro 3 tyče.
        return hanojske_veze_3(pocet_disku, start, cil, tyce)
    if len(tyce) >= 3 and pocet_disku > 0:
        nej_reseni = float("inf")
        nej_skore = float("inf")
        for k in range(1, pocet_disku):
            pomocne_tyce = list(tyce.difference([start, cil]))    # vytvoří list pomocných tyčí
            krok_2 = hanojske_veze_4(k, start, pomocne_tyce[0], tyce)   # krok 2 wiki algoritmu
            zbyla_tyc = tyce.difference([pomocne_tyce[0]])
            krok_3 = hanojske_veze_4(pocet_disku - k, start, cil, zbyla_tyc)    # krok 3 algoritmu
            krok_4 = hanojske_veze_4(k, pomocne_tyce[0], cil, tyce)     # krok 4 algoritmu
            pohyby = krok_2 + krok_3 + krok_4
            if len(pohyby) < nej_skore:
                nej_reseni = pohyby
                nej_skore = len(pohyby)
            if nej_skore < float("inf"):
                return nej_reseni
    return None


print(hanojske_veze_4(6))
