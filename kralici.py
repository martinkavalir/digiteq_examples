"""Prezentace výpočtu množení králíků."""


def mnozeni_kraliku(_n):
    """Funkce založená na fibonnaciho posloupnosti a slouží k výpočtu růstu populace králíků."""
    if _n <= 2:
        return 1
    return mnozeni_kraliku(_n-1)+mnozeni_kraliku(_n-2)


print(mnozeni_kraliku(5))
