"""Slouží k testování."""
import unittest
from unsigned_short_float import bin_to_float


class TestDataOperations(unittest.TestCase):
    """Slouží k otestování převedu čísla z jednoho datového typu na druhý."""

    def test_bin_to_float(self):
        """Testovací metoda pro převod z binárního čísla na float."""
        self.assertEqual(bin_to_float('111100000000000'), 32768.0)


if __name__ == '__main__':
    unittest.main()
