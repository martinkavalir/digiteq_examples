"""Příklad na převedení výstupu z RVP8 na float."""
import struct


def bin_to_float(binary):
    """Konvertuje unsigned short na float."""
    return struct.unpack('!e', struct.pack('!H', int(binary, 2)))[0]


print(bin_to_float('111100000000000'))
